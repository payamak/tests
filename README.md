# Payamak Tests

> Testing components for Payamak SMS messaging library

[![Packagist Version](https://img.shields.io/packagist/v/payamak/tests.svg)][1]
[![PHP from Packagist](https://img.shields.io/packagist/php-v/payamak/tests.svg)][2]
[![Travis (.com) branch](https://img.shields.io/gitlab/pipeline/payamak/payamak/master)][3]
[![Codecov](https://img.shields.io/codecov/c/gl/payamak/tests.svg)][4]
[![Packagist](https://img.shields.io/packagist/l/payamak/tests.svg)][5]
[![Twitter: nekofar](https://img.shields.io/twitter/follow/nekofar.svg?style=flat)][6]

[1]: https://packagist.org/packages/payamak/tests
[2]: https://www.php.net/releases/7_2_0.php
[3]: https://gitlab.com/payamak/tests
[4]: https://codecov.io/gl/payamak/tests
[5]: https://gitlab.com/payamak/tests/blob/master/LICENSE
[6]: https://twitter.com/nekofar