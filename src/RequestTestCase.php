<?php


namespace Payamak\Tests;


use Payamak\Common\Message\AbstractRequest;

class RequestTestCase extends TestCase
{
    /**
     * @var AbstractRequest
     */
    protected $request;

    /**
     * @test
     */
    public function testDefaultParametersHaveMatchingMethods()
    {
        $parameters = $this->request->getData();
        foreach ($parameters as $key => $default) {
            $getter = 'get' . ucfirst($this->camelCase($key));
            $setter = 'set' . ucfirst($this->camelCase($key));
            $value = $this->getMockValue($default);

            $this->assertTrue(method_exists($this->request, $getter), "Request must implement $getter()");
            $this->assertTrue(method_exists($this->request, $setter), "Request must implement $setter()");

            // setter must return instance
            $this->assertSame($this->request, $this->request->$setter($value));
            $this->assertSame($value, $this->request->$getter());
        }
    }
}