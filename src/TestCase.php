<?php

/**
 * @package Payamak\Tests
 */

namespace Payamak\Tests;

use Http\Client\Common\HttpMethodsClient;
use Http\Client\HttpClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Mock\Client as MockClient;
use Psr\Http\Message\ResponseInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionObject;
use function GuzzleHttp\Psr7\parse_response;


/**
 * Class TestCase
 */
abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @var MockClient
     */
    private $mockClient;

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var HttpMethodsClient
     */
    private $methodsClient;

    /**
     * @return MockClient
     */
    public function getMockClient(): MockClient
    {
        if (is_null($this->mockClient)) {
            $this->mockClient = new MockClient();
        }

        return $this->mockClient;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient(): HttpClient
    {
        if (is_null($this->httpClient)) {
            $this->httpClient = HttpClientDiscovery::find();
        }

        return $this->httpClient;
    }

    /**
     * @return HttpMethodsClient
     */
    public function getMethodsClient(HttpClient $httpClient): HttpMethodsClient
    {
        if (is_null($this->methodsClient)) {
            $this->methodsClient = new HttpMethodsClient(
                $httpClient,
                MessageFactoryDiscovery::find()
            );
        }
        return $this->methodsClient;
    }

    /**
     * Get all of the mocked requests
     *
     * @return array
     */
    public function getMockedRequests()
    {
        return $this->mockClient->getRequests();
    }

    /**
     * Get a mock response for a client by mock file name
     *
     * @param string $path Relative path to the mock response file
     *
     * @return ResponseInterface
     */
    public function getMockHttpResponse($path)
    {
        if ($path instanceof ResponseInterface) {
            return $path;
        }

        $ref = new ReflectionObject($this);
        $dir = dirname($ref->getFileName());

        // if mock file doesn't exist, check parent directory
        if (!file_exists($dir . '/Mock/' . $path) && file_exists($dir . '/../Mock/' . $path)) {
            return parse_response(file_get_contents($dir . '/../Mock/' . $path));
        }

        return parse_response(file_get_contents($dir . '/Mock/' . $path));
    }

    /**
     * Set a mock response from a mock file on the next client request.
     *
     * This method assumes that mock response files are located under the
     * Mock/ subdirectory of the current class. A mock response is added to the next
     * request sent by the client.
     *
     * An array of path can be provided and the next x number of client requests are
     * mocked in the order of the array where x = the array length.
     *
     * @param array|string $paths Path to files within the Mock folder of the service
     *
     * @return void returns the created mock plugin
     */
    public function setMockHttpResponse($paths)
    {
        foreach ((array)$paths as $path) {
            $this->mockClient->addResponse($this->getMockHttpResponse($path));
        }
    }

    /**
     * @param $object
     * @param $property
     * @param $value
     * @throws ReflectionException
     */
    protected function setProtectedProperty($object, $property, $value): void
    {
        $reflection = new ReflectionClass($object);
        $reflection_property = $reflection->getProperty($property);
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($object, $value);
    }

    /**
     * @param $object
     * @param $property
     * @return mixed
     * @throws ReflectionException
     */
    public function getProtectedProperty($object, $property)
    {
        $reflection = new ReflectionClass($object);
        $reflection_property = $reflection->getProperty($property);
        $reflection_property->setAccessible(true);
        return $reflection_property->getValue($object);
    }

    /**
     * Converts a string to camel case
     *
     * @param string $str
     * @return string
     */
    public function camelCase($str)
    {
        return preg_replace_callback(
            '/_([a-z])/',
            function ($match) {
                return strtoupper($match[1]);
            },
            $str
        );
    }

    /**
     * Generate random value by type
     *
     * @param $value
     * @return mixed
     */
    public function getMockValue($value)
    {
        switch (gettype($value)) {
            case 'boolean':
                return false;
                break;
            case 'integer':
                return rand(10, 100);
                break;
            case 'array':
                return [1,2,3,4,5];
                break;
            case 'object':
                return new \stdClass();
            default:
                return uniqid();
        }
    }
}