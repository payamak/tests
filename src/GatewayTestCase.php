<?php

/**
 * @package Payamak\Tests
 */

namespace Payamak\Tests;

use Payamak\Common\AbstractGateway;
use Payamak\Common\Message\RequestInterface;

/**
 * Base Gateway Test class
 *
 * Ensures all gateways conform to consistent standards
 */
abstract class GatewayTestCase extends TestCase
{
    /**
     * @var AbstractGateway
     */
    protected $gateway;

    /**
     * @test
     */
    public function testGetNameNotEmpty()
    {
        $name = $this->gateway->getName();
        $this->assertNotEmpty($name);
        $this->assertIsString($name);
    }

    /**
     * @test
     */
    public function testGetShortNameNotEmpty()
    {
        $shortName = $this->gateway->getShortName();
        $this->assertNotEmpty($shortName);
        $this->assertIsString($shortName);
    }

    /**
     * @test
     */
    public function testGetDefaultParametersReturnsArray()
    {
        $settings = $this->gateway->getDefaultParameters();
        $this->assertIsArray($settings);
    }

    /**
     * @test
     */
    public function testDefaultParametersHaveMatchingMethods()
    {
        $parameters = $this->gateway->getDefaultParameters();
        foreach ($parameters as $key => $default) {
            $getter = 'get' . ucfirst($this->camelCase($key));
            $setter = 'set' . ucfirst($this->camelCase($key));
            $value = $this->getMockValue($default);

            $this->assertTrue(method_exists($this->gateway, $getter), "Gateway must implement $getter()");
            $this->assertTrue(method_exists($this->gateway, $setter), "Gateway must implement $setter()");

            // setter must return instance
            $this->assertSame($this->gateway, $this->gateway->$setter($value));
            $this->assertSame($value, $this->gateway->$getter());
        }
    }

    /**
     * @test
     */
    public function testTestMode()
    {
        $this->assertSame($this->gateway, $this->gateway->setTestMode(false));
        $this->assertSame(false, $this->gateway->getTestMode());

        $this->assertSame($this->gateway, $this->gateway->setTestMode(true));
        $this->assertSame(true, $this->gateway->getTestMode());
    }

    /**
     * @test
     */
    public function testSupportsBalance()
    {
        $supportsBalance = $this->gateway->supportsBalance();
        $this->assertIsBool($supportsBalance);

        if ($supportsBalance) {
            $this->assertInstanceOf(RequestInterface::class, $this->gateway->balance());
        } else {
            $this->assertFalse(method_exists($this->gateway, 'balance'));
        }
    }

    /**
     * @test
     */
    public function testBalanceParameters()
    {
        $supportsBalance = $this->gateway->supportsBalance();
        $this->assertIsBool($supportsBalance);

        if ($supportsBalance) {
            foreach ($this->gateway->getDefaultParameters() as $key => $default) {
                // set property on gateway
                $getter = 'get' . ucfirst($this->camelCase($key));
                $setter = 'set' . ucfirst($this->camelCase($key));
                $value = $this->getMockValue($default);
                $this->gateway->$setter($value);

                // request should have matching property, with correct value
                $request = $this->gateway->balance();
                $this->assertSame($value, $request->$getter());
            }
        }
    }

    /**
     * @test
     */
    public function testSupportsMessage()
    {
        $supportsMessage = $this->gateway->supportsMessage();
        $this->assertIsBool($supportsMessage);

        if ($supportsMessage) {
            $this->assertInstanceOf(RequestInterface::class, $this->gateway->message());
        } else {
            $this->assertFalse(method_exists($this->gateway, 'message'));
        }
    }

    /**
     * @test
     */
    public function testMessageParameters()
    {
        $supportsMessage = $this->gateway->supportsMessage();
        $this->assertIsBool($supportsMessage);

        if ($supportsMessage) {
            foreach ($this->gateway->getDefaultParameters() as $key => $default) {
                // set property on gateway
                $getter = 'get' . ucfirst($this->camelCase($key));
                $setter = 'set' . ucfirst($this->camelCase($key));
                $value = $this->getMockValue($default);
                $this->gateway->$setter($value);

                // request should have matching property, with correct value
                $request = $this->gateway->message();
                $this->assertSame($value, $request->$getter());
            }
        }
    }

    /**
     * @test
     */
    public function testSupportsDeliver()
    {
        $supportsDeliver = $this->gateway->supportsDeliver();
        $this->assertIsBool($supportsDeliver);

        if ($supportsDeliver) {
            $this->assertInstanceOf(RequestInterface::class, $this->gateway->deliver());
        } else {
            $this->assertFalse(method_exists($this->gateway, 'deliver'));
        }
    }

    /**
     * @test
     */
    public function testDeliverParameters()
    {
        $supportsDeliver = $this->gateway->supportsDeliver();
        $this->assertIsBool($supportsDeliver);

        if ($supportsDeliver) {
            foreach ($this->gateway->getDefaultParameters() as $key => $default) {
                // set property on gateway
                $getter = 'get' . ucfirst($this->camelCase($key));
                $setter = 'set' . ucfirst($this->camelCase($key));
                $value = $this->getMockValue($default);
                $this->gateway->$setter($value);

                // request should have matching property, with correct value
                $request = $this->gateway->deliver();
                $this->assertSame($value, $request->$getter());
            }
        }
    }
}